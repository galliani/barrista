# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170416100430) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "custom_auto_increments", force: :cascade do |t|
    t.string   "counter_model_name"
    t.integer  "counter",            default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["counter_model_name"], name: "index_custom_auto_increments_on_counter_model_name", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "invoices", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "trade_id"
    t.integer  "provider"
    t.string   "provider_invoice_id"
    t.string   "amount"
    t.string   "billable_amount"
    t.string   "currency"
    t.string   "invoice_url"
    t.string   "status"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["provider"], name: "index_invoices_on_provider", using: :btree
    t.index ["provider_invoice_id"], name: "index_invoices_on_provider_invoice_id", using: :btree
    t.index ["trade_id"], name: "index_invoices_on_trade_id", using: :btree
  end

  create_table "payments", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "trade_id"
    t.integer  "sender_identifier_type",    default: 0
    t.string   "sender_identifier"
    t.integer  "recipient_identifier_type"
    t.string   "recipient_identifier"
    t.string   "total_paid",                            null: false
    t.datetime "paid_at"
    t.string   "fees_paid"
    t.string   "net_paid"
    t.string   "currency"
    t.string   "label"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["trade_id"], name: "index_payments_on_trade_id", using: :btree
  end

  create_table "trades", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "object_name"
    t.string   "object_identifier"
    t.string   "callback_url"
    t.integer  "initiator"
    t.integer  "status",            default: 0
    t.integer  "category",          default: 0
    t.string   "label"
    t.datetime "deleted_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["deleted_at"], name: "index_trades_on_deleted_at", using: :btree
    t.index ["object_identifier"], name: "index_trades_on_object_identifier", using: :btree
    t.index ["object_name"], name: "index_trades_on_object_name", using: :btree
    t.index ["status"], name: "index_trades_on_status", using: :btree
  end

  add_foreign_key "invoices", "trades"
  add_foreign_key "payments", "trades"
end
