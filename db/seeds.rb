t = Trade.new(
  object_name: 'Prepaid::Sale',
  object_identifier: 'a5093dd9-cb33-5783-b01c-0d0d381490f1',
  initiator: 0,
  status: 0,
  callback_url: 'http://card.lorem/sales/a5093dd9-cb33-5783-b01c-0d0d381490f1/callback'
)

t.save

Invoice.create(
  trade: t,
  provider: 0,
  amount: "500000",
  currency: "idr"
)