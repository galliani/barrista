class CreateTrades < ActiveRecord::Migration[5.0]
  def self.up
    create_table :trades, id: :uuid do |t|
      t.string      :object_name, index: true
      t.string      :object_identifier, index: true
      t.string      :callback_url
      t.integer     :initiator
      t.integer     :status, default: 0, index: true
      t.integer     :category, default: 0
      t.string      :label
      t.datetime    :deleted_at, index: true

      t.timestamps
    end
  end

  def self.down
    drop_table :trades
  end
end
