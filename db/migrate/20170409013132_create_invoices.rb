class CreateInvoices < ActiveRecord::Migration[5.0]
  def self.up
    create_table :invoices, id: :uuid do |t|
      t.references    :trade, foreign_key: true, type: :uuid
      t.integer       :provider, index: true
      t.string        :provider_invoice_id, index: true
      t.string        :amount
      t.string        :billable_amount
      t.string        :currency
      t.string        :invoice_url
      t.string        :status

      t.timestamps
    end
  end

  def self.down
    drop_table :invoices
  end
end
