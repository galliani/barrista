class CreatePayments < ActiveRecord::Migration[5.0]
  def self.up
    create_table :payments, id: :uuid do |t|
      t.references    :trade, foreign_key: true, type: :uuid      
      t.integer       :sender_identifier_type, default: 0
      t.string        :sender_identifier
      t.integer       :recipient_identifier_type
      t.string        :recipient_identifier
      t.string        :total_paid, null: false
      t.datetime      :paid_at
      t.string        :fees_paid
      t.string        :net_paid
      t.string        :currency
      t.string        :label

      t.timestamps
    end
  end

  def self.down
    drop_table :payments
  end
end
