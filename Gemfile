source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.2'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18.4'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

gem 'figaro', '1.1.1'

# API building blocks
# gem 'wisper', '2.0.0'
gem 'active_interaction', '~> 3.5'
gem 'active_model_serializers', '0.10.2'
gem 'kaminari', '~> 0.16.3'

# Key functionality
gem 'xendit_api', '0.2.0'
gem 'delayed_job_active_record', '4.1.1'
gem 'protokoll',  '2.0.1'
gem 'faraday', '~> 0.11.0'
### Enable Cross-Origin Resource Sharing
gem 'rack-cors', '0.4.0', require: 'rack/cors'

# Supporting
gem 'slack-notifier', '2.1.0'
gem 'sendinblue', '~> 2.4'
gem 'stoplight', '1.4.0'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'thin', '1.5.1'
end

group :development do
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :test do
  gem 'capybara', '2.5.0'
  gem 'launchy', '2.4.3'
  gem 'minitest-reporters', '1.1.12'
  gem 'shoulda-context', '1.2.2'
  gem 'shoulda-matchers', '3.1.1'
  gem 'webmock', '2.1.0'  
end

group :production do
  # Use Puma as the app server
  gem 'puma', '~> 3.0'
end
