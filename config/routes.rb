Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    scope '/barrista' do
      namespace :v1 do
        resources :trades, only: [:index, :show, :create] do
          resources :payments, only: [:index]
        end
        resources :invoices, only: [:index, :show]
        resources :payments, only: [:show]
        resources :xendit_callbacks, only: [] do
          collection do
            post 'invoice'
          end
        end
      end
    end
  end

  root to: 'rails/welcome#index'
end
