module RequestsHelper
  def get_with_valid_header(path, params={}, headers={})
    headers.merge!('Accept' => Mime[:jsonapi])
    get path, params, headers
  end

  def post_with_valid_header(path, params={}, headers={})
    # headers.merge!('ACCEPT' => Mime[:jsonapi], 'CONTENT_TYPE' => Mime[:jsonapi])
    post path, params, headers
  end
end