module JsonapiTestHelper
  # HELPER methods
  def setup_valid_request(authentication: true)
    @request.headers['Accept'] = 'application/vnd.api+json'
    setup_correct_authentication unless !authentication
  end

  def setup_xendit_callback_token_in_header(token = ENV['XENDIT_VALIDATION_TOKEN'])
    @request.headers['X-CALLBACK-TOKEN'] = token
  end

  def setup_correct_authentication
    @request.headers['Authorization'] = ActionController::HttpAuthentication::Token.encode_credentials(ENV['API_TOKEN'] )
  end

  def jsondata
    JSON.parse response.body
  end

  def data_hash
    jsondata['data']
  end

  def attributes_hash
    data_hash['attributes']
  end

  def attribute_element(key:)
    data_hash['attributes'][key]
  end
end