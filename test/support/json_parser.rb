require 'json'

def parsed_json
  JSON.parse(response.body)
end

# This is to check how many objects of json without root retrieved
def count_json_to_eq(number)
  expect(parsed_json.count).to eq number
end

def expected_json_response(field, value)
  expect(parsed_json.collect{|l| l[field]}).to include(value)
end

def not_expected_json_response(field, value)
  expect(parsed_json.collect{|l| l[field]}).not_to include(value)
end

# V1 requirements
def jsondata
  JSON.parse response.body
end

def data_hash
  jsondata['data']
end

def attributes_hash
  data_hash['attributes']
end

def links_hash
  jsondata['links']
end

def attribute_element(key:)
  data_hash['attributes'][key]
end

def relationship_data(resource:, key:)
  data_hash['relationships'][resource]['data'][key]
end

def load_and_setup_json_input(filename:)
  JSON.parse(file_fixture(filename).read)
end

def response_status_should_be(code)
  expect(response.status).to eq code
end

def set_header_variable(key:, value:)
  @request.headers[key] = value
end
