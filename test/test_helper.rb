ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/autorun'
# Capybara and shoulda for more verbose
require 'capybara/rails'
require 'shoulda/context'
# Improved Minitest output (color and progress bar)
require 'minitest/reporters'
require 'json'
require 'webmock/minitest'
WebMock.disable_net_connect!

# require support files in the support folders
Dir[Rails.root.join("test/support/**/*")].each { |f| require f }

Minitest::Reporters.use!(
  Minitest::Reporters::ProgressReporter.new,
  ENV,
  Minitest.backtrace_filter
)

module StubEachTest
  def before_setup
    super

    # Stub Slack Notifier
    stub_request(:post, ENV['SLACK_WEBHOOK_URL']).
      to_return(:status => 200, :body => "", :headers => {})
  end
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include ValidityTestHelper
  include StubEachTest
  include JsonapiTestHelper
end

class ActionDispatch::IntegrationTest
  include Capybara::DSL
end
