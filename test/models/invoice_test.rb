require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase
  def setup
    @persisted_record = invoices(:card_sale_one_invoice)
    @new_record = Invoice.new
  end

  test 'valid user' do
    @persisted_record.save

    assert_valid @persisted_record
  end

  test 'presence validations' do
    @new_record.valid?

    attrs = [:trade_id, :provider, :amount]

    attrs.each do |att|
      assert_equal true, @new_record.errors.messages.key?(att)
    end 
  end

  test 'attributes' do
    assert_respond_to @new_record, :provider
    assert_respond_to @new_record, :provider_invoice_id
    assert_respond_to @new_record, :amount
    assert_respond_to @new_record, :currency
    assert_respond_to @new_record, :invoice_url
    assert_respond_to @new_record, :status

    # enums
    assert_equal @persisted_record.provider, 'xendit'
  end
end
