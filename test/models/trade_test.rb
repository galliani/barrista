require 'test_helper'

class TradeTest < ActiveSupport::TestCase
  def setup
    @persisted_record = trades(:card_sale_one)
    @new_record = Trade.new
  end

  test 'valid record' do
    @new_record.attributes = @persisted_record.attributes.except!(
      'id', 'created_at', 'updated_at'
    )
    @new_record.save


    assert_valid @new_record
    assert_equal true, @new_record.label.present?
  end

  test 'presence validations' do
    @new_record.valid?

    attrs = [:object_name, :object_identifier, :callback_url, :initiator]

    attrs.each do |att|
      assert_equal true, @new_record.errors.messages.key?(att)
    end 
  end

  test 'attributes' do
    assert_respond_to @new_record, :object_name
    assert_respond_to @new_record, :object_identifier
    assert_respond_to @new_record, :callback_url
    assert_respond_to @new_record, :initiator
    assert_respond_to @new_record, :status
    assert_respond_to @new_record, :deleted_at
    assert_respond_to @new_record, :invoice
    assert_respond_to @new_record, :payments
    assert_respond_to @new_record, :label

    # enums
    assert_equal @persisted_record.status, 'waiting'
    assert_equal @persisted_record.initiator, 'carta'
  end
end
