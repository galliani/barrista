require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  def setup
    @new_record = Payment.new
    @trade = trades(:card_sale_one)
  end

  test 'valid record' do
    @persisted_record = Payment.new(
      trade: @trade,
      sender_identifier_type: 0,
      sender_identifier: 'payer_email@lorem.com',
      # recipient_identifier: when the recipient is nil, it means the system is the recipient
      total_paid: '540000',
      fees_paid: '1500',
      net_paid: '500',
      currency: 'idr',
      paid_at: DateTime.current
    )
    @persisted_record.save

    assert_valid @persisted_record
    assert_equal true, @persisted_record.label.present?
  end

  test 'presence validations' do
    @new_record.valid?

    attrs = [:sender_identifier, :total_paid, :paid_at]

    attrs.each do |att|
      assert_equal true, @new_record.errors.messages.key?(att)
    end 
  end

  test 'attributes' do
    assert_respond_to @new_record, :sender_identifier
    assert_respond_to @new_record, :sender_identifier_type
    assert_respond_to @new_record, :recipient_identifier
    assert_respond_to @new_record, :recipient_identifier_type
    assert_respond_to @new_record, :total_paid
    assert_respond_to @new_record, :paid_at
    assert_respond_to @new_record, :fees_paid
    assert_respond_to @new_record, :net_paid
    assert_respond_to @new_record, :trade
    assert_respond_to @new_record, :trade_invoice
  end
end
