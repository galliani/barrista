require 'test_helper'
require 'webmock/minitest'

class API::V1::InvoicesControllerTest < ActionController::TestCase
  def setup
    @persisted_record = invoices(:card_sale_one_invoice)
  end
  # :index
  test 'retrieving a collection of records' do
    setup_valid_request

    get :index

    assert_response :ok
    assert_equal Mime[:jsonapi], response.content_type
    # Ensure that the response contains a hash key of events inside data hash
    assert_equal data_hash[0]['type'], 'invoices'
  end

  test 'get #index without authentication' do
    setup_valid_request(authentication: false)

    get :index
    # Make sure that without correct accept header, the request is not acceptable
    assert_equal 401, response.status
  end

  test 'get #index without correct Headers' do
    setup_correct_authentication

    get :index
    # Make sure that without correct accept header, the request is not acceptable
    assert_equal 406, response.status
  end

  # :show
  test 'retrieving a single record' do
    setup_valid_request

    get :show, params: { id: @persisted_record.id }

    assert_response :ok

    record = JSON.parse response.body

    assert_equal 200, response.status
    assert_equal 'invoices', record['data']['type']

    attrs = [
      'provider', 'provider-invoice-id', 'amount', 'currency', 
      'invoice-url', 'status'
    ]
    assert_equal attrs, record['data']['attributes'].keys
  end

  test 'retrieving a single record that DOES NOT EXIST' do
    setup_valid_request

    get :show, params: { id: SecureRandom.uuid }

    assert_response :not_found
    assert_equal 'null', response.body
  end
end
