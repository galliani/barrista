require 'test_helper'
require 'webmock/minitest'

class API::V1::TradesControllerTest < ActionController::TestCase
  def setup
    @persisted_record = trades(:card_sale_one)
  end
  # :index
  test 'retrieving a collection of records' do
    setup_valid_request

    get :index

    assert_response :ok
    assert_equal Mime[:jsonapi], response.content_type
    # Ensure that the response contains a hash key of events inside data hash
    assert_equal data_hash[0]['type'], 'trades'
  end

  test 'get #index without authentication' do
    setup_valid_request(authentication: false)

    get :index
    # Make sure that without correct accept header, the request is not acceptable
    assert_equal 401, response.status
  end

  test 'get #index without correct Headers' do
    setup_correct_authentication

    get :index
    # Make sure that without correct accept header, the request is not acceptable
    assert_equal 406, response.status
  end

  # :show
  test 'retrieving a single record' do
    setup_valid_request

    get :show, params: { id: @persisted_record.id }

    assert_response :ok

    record = JSON.parse response.body

    assert_equal 200, response.status
    assert_equal 'trades', record['data']['type']

    attrs = ['object-name', 'object-identifier', 'initiator', 'status', 'callback-url']
    assert_equal attrs, record['data']['attributes'].keys
  end

  test 'retrieving a single record that DOES NOT EXIST' do
    setup_valid_request
    
    get :show, params: { id: SecureRandom.uuid }

    assert_response :not_found
    assert_equal 'null', response.body
  end

  # CREATE
  test "Creating new event without correct content-type" do
    post :create, params: {}
    assert_response 406
  end

  test "Creating new event with invalid data" do
    setup_valid_request

    # Load the fixture json input and then parse it
    data_hash = load_and_setup_json_input(filename: 'invalid_trade_post_input.json')

    # Make the request!
    post :create, params: { data: data_hash }

    assert_response 422
    # Check for error object
    pointers = jsondata['errors'].collect { |e|
      e['source']['pointer'].split('/').last
    }.sort
    assert_equal ['attributes'], pointers
  end

  test 'receiving valid trade callback' do      
    data = file_fixture('card_trade_post_input.json').read
    stub = stub_request(:post, ENV['XENDIT_API_URL'] + '/v2/invoices')
      .to_return(:status => 201, :body => data, :headers => {})

    setup_valid_request

    post :create, params: { data: JSON.parse(data) }

    assert_response 201
    assert_equal Mime[:jsonapi], response.content_type

    record = JSON.parse response.body
    # Ensure that the response contains a hash key of events inside data hash
    assert_equal 'trades', record['data']['type']
  end
end
