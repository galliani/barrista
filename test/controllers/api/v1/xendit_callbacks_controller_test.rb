require 'test_helper'
require 'webmock/minitest'

class API::V1::XenditCallbacksControllerTest < ActionController::TestCase
  def setup
    @valid_internal_invoice = invoices(:card_sale_one_invoice)
    @valid_internal_invoice.update(
      provider_invoice_id: '579c8d61f23fa4ca35e52da4',
      amount: '500000'
    )    
  end

  # :invoice
  test 'with wrong validation token' do
    data = file_fixture('valid_xendit_invoice_callback.json').read

    setup_valid_request

    post :invoice, params: { data: data }

    assert_response 401
  end

  test 'receiving invoice callback with empty data' do
    data = file_fixture('invalid_trade_post_input.json').read

    setup_valid_request
    setup_xendit_callback_token_in_header

    post :invoice, params: { data: data }

    assert_response 422
  end

  test 'receiving valid invoice callback' do      
    data = file_fixture('valid_xendit_invoice_callback.json').read
    stub = stub_request(:post, @valid_internal_invoice.trade.callback_url)
      .to_return(:status => 200, :body => '{}', :headers => {})

    setup_valid_request
    setup_xendit_callback_token_in_header

    post :invoice, params: { data: data }

    assert_response 200
    assert_equal Mime[:jsonapi], response.content_type

    record = JSON.parse response.body
    # Ensure that the response contains a hash key of events inside data hash
    assert_equal 'payments', record['data']['type']
  end
end
