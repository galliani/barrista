require 'test_helper'

class API::V1::ProcessXenditInvoiceTest < ActiveSupport::TestCase
  def setup
    @valid_internal_invoice = invoices(:card_sale_one_invoice)
    @valid_internal_invoice.update(
      provider_invoice_id: 'a5093dd9-cb33-5783-b01c-0d0d381490f1',
      amount: '500000'
    )
    @valid_input = {
      id: @valid_internal_invoice.provider_invoice_id,
      external_id: @valid_internal_invoice.id,
      status: 'COMPLETED',
      payer_email: 'payer@lorem.com',
      payment_method: 'POOL',
      paid_amount: 54000,
      adjusted_received_amount: 47500,
      adjusted_xendit_fee_amount: 500,
      updated: '2016-10-10T08:15:03.404Z'
    }
  end

  test 'generates payment record' do
    @service = API::V1::ProcessXenditInvoice.run(@valid_input)

    # Testing the outcome
    payment = @service.result

    assert_equal true, payment.id.present?
    assert_equal '54000', payment.total_paid
    assert_equal @valid_input[:status], payment.trade_invoice.status
  end
end