require 'test_helper'

class API::V1::CreateXenditInvoiceTest < ActiveSupport::TestCase
  def setup
    @valid_internal_invoice = invoices(:card_sale_one_invoice)    
  end

  test 'sends request to create invoice to xendit then update record' do
    # Load the fixture json input and then parse it
    data_hash = {
      invoice: @valid_internal_invoice,
      external_id: 'internal_invoice_no',
      payer_email: 'sample@lorem.com',
      description: 'payment for X',
      amount:      '50000'
    }

    data = file_fixture('xendit_invoice.json').read
    stub = stub_request(:post, ENV['XENDIT_API_URL'] + '/v2/invoices')
      .to_return(:status => 201, :body => data, :headers => {})

    @service = API::V1::CreateXenditInvoice.run(data_hash)

    # Testing the outcome
    invoice = @service.result
    
    # assert_equal true, @service.result.provider_invoice_id.present?
    # assert_equal true, @service.result.invoice_url.present?
    # assert_equal true, @service.result.status.present?
  end
end