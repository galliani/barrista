require 'test_helper'

class API::V1::TradeRecordingTest < ActiveSupport::TestCase
  def setup
    @origin_count = Trade.count
  end

  test 'DOES NOT generate trade when passed an invalid input' do
    # Load the fixture json input and then parse it
    data_hash = load_and_setup_json_input(filename: 'invalid_trade_post_input.json')

    @service = API::V1::TradeRecording.run(data_hash.merge(trade: Trade.new))

    # Ensure that the service has no errors
    assert_equal true, @service.errors.present?

    # Ensure that the number of trades has NOT changed
    assert_equal Trade.count, @origin_count
  end

  test 'generates trade and associated resources when passed a valid input' do
    data = file_fixture('xendit_invoice.json').read
    stub = stub_request(:post, ENV['XENDIT_API_URL'] + '/v2/invoices')
      .to_return(:status => 201, :body => data, :headers => {})
    
    # Load the fixture json input and then parse it
    data = file_fixture('card_trade_post_input.json').read
    data_hash = JSON.parse(data)
    @service = API::V1::TradeRecording.run(data_hash.merge(trade: Trade.new))

    # Ensure that the service has no errors
    assert_equal false, @service.errors.present?

    # Ensure that the number of trades has changed as the result
    assert_equal Trade.count, @origin_count + 1

    # Testing the outcome
    trade = @service.result
    att = data_hash['attributes']

    assert_equal trade.class.name, 'Trade'
    assert_equal true, trade.id.present?
    assert_equal att['object_name'], trade.object_name
    assert_equal att['object_identifier'], trade.object_identifier
    assert_equal att['callback_url'], trade.callback_url
    assert_equal true, trade.invoice.present?
  end
end