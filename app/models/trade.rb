class Trade < ApplicationRecord
  has_one :invoice
  has_many :payments

  protokoll :label, :pattern => "%y####TR###%m"

  enum initiator: [:carta]
  enum category: [:to_system]
  enum status: [:waiting, :processing, :completed, :cancelled]

  validates :object_name, :object_identifier, :callback_url, :initiator,
            presence: true
end
