class Payment < ApplicationRecord
  belongs_to :trade

  delegate  :invoice, :callback_url,
            to: :trade, allow_nil: true, prefix: true

  protokoll :label, :pattern => "%y####PA###%m"
  
  enum sender_identifier_type: [:sender_email, :sender_mobile_number]
  enum recipient_identifier_type: [:recipient_email, :recipient_mobile_number]

  validates :sender_identifier, :sender_identifier_type, :total_paid, :paid_at,
            presence: true  
end
