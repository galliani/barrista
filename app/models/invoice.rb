class Invoice < ApplicationRecord
  belongs_to :trade

  delegate  :payments, :label, 
            to: :trade, allow_nil: true, prefix: true

  enum provider: [:xendit]  

  validates :trade_id, :provider, :amount,
            presence: true
end
