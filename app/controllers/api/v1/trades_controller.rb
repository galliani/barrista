module API
  module V1
    class TradesController < BaseController
      def index
        presenter = API::V1::TradesPresenter.new(
          controller: self, filters: params[:filter]
        )

        render meta: presenter.meta,
               json: presenter.data,
               links: presenter.links,
               each_serializer: ::API::V1::TradeSerializer,
               status: :ok
      end

      def show
        find_record; return if performed?

        render  json: @record,
                serializer: ::API::V1::TradeSerializer,
                include: parse_inclusion_params,
                status: :ok
      end

      def create
        service = TradeRecording.run(params[:data].merge(trade: Trade.new))

        # if the service runs smooth then it should have no errors
        if service.valid?
          notify_via_slack('A TRADE record has been created')
          render  json: service.result,
                  serializer: ::API::V1::TradeSerializer,
                  include: parse_inclusion_params,
                  location: api_v1_trade_url(service.result),                  
                  status: 201
        else
          notify_via_slack('FAILED to create a TRADE record')
          # if there are errors, render it using the helper method
          render_error(resource: service, status: 422)
        end  
      end

      def update
      end

      private

      def find_record
        @record = Trade.find_by(id: params[:id])

        render status: :not_found, json: nil && return if @record.nil?
      end
    end
  end
end