module API
  module V1
    class BaseController < ActionController::API
      include ActionController::HttpAuthentication::Token::ControllerMethods

      before_action :prevent_request_unless_in_jsonapi_format,
                    :authenticate
      after_action :set_content_type
      
      # This return 406 status code if the headers do not contain Accept variable
      # set to jsonapi format
      def prevent_request_unless_in_jsonapi_format
        if request.headers['Accept'] != Mime[:jsonapi]
          render body: nil, status: 406
        end
      end

      def set_content_type
        self.content_type = Mime[:jsonapi]
      end

      # This one serialize errors and then render it
      def render_error(resource:, status: 422)
        render json: resource, status: status, adapter: :json_api,
               serializer: ActiveModel::Serializer::ErrorSerializer
      end

      def parse_inclusion_params
        params['include'].split(',') if params['include']
      end

      def authenticate
        authenticate_or_request_with_http_token do |token, options|
          # Compare the tokens in a time-constant manner, to mitigate
          # timing attacks. Read more: 
          # https://thisdata.com/blog/timing-attacks-against-string-comparison/
          ActiveSupport::SecurityUtils.secure_compare(
            ::Digest::SHA256.hexdigest(ENV['API_TOKEN']),
            ::Digest::SHA256.hexdigest(token)
          )
        end
      end

      def notify_via_slack(message, webhook_url = nil, username = nil)
        SendMessageToSlack.run(
          message: message, 
          slack_webhook_url: webhook_url, 
          slack_notif_username: username
        )
      end
    end
  end
end