module API
  module V1
    class InvoicesController < BaseController
      def index
        presenter = API::V1::InvoicesPresenter.new(
          controller: self, filters: params[:filter]
        )

        render meta: presenter.meta,
               json: presenter.data,
               links: presenter.links,
               each_serializer: ::API::V1::InvoiceSerializer,
               status: :ok        
      end

      def show
        find_record; return if performed?

        render  json: @record,
                serializer: ::API::V1::InvoiceSerializer,
                include: parse_inclusion_params,
                status: :ok       
      end

      private

      def find_record
        @record = Invoice.find_by(id: params[:id])

        render status: :not_found, json: nil && return if @record.nil?
      end     
    end
  end
end