module API
  module V1
    class XenditCallbacksController < BaseController
      before_action :check_validity_of_request
      
      def invoice
        notify_via_slack('Received invoice callback from Xendit')

        parsed_params = JSON.parse params[:data]

        service = API::V1::ProcessXenditInvoice.run(parsed_params)
        result = service.result

        # if the service runs smooth then it should have no errors
        if service.errors.blank?
          response = pass_invoice_callback_to_client(
            client_url: result.trade_callback_url, invoice: result.trade_invoice
          )

          if response.success?
            notify_via_slack('Successfully passed invoice callback to Client App')
            
            render  json: result,
                    serializer: ::API::V1::PaymentSerializer,
                    include: parse_inclusion_params,
                    status: :ok
          else
            render body: 'Failed to pass the callback to client', 
                   status: 422
          end
        else
          # if there are errors, render it using the helper method
          render_error(resource: service, status: 422)
        end
      end

      private

      def check_validity_of_request
        if request.headers['X-CALLBACK-TOKEN'] != ENV['XENDIT_VALIDATION_TOKEN']
          render body: 'Invalid token', status: 401
        end
      end

      def pass_invoice_callback_to_client(client_url:, invoice:)
        @connection = Faraday.new do |faraday|
          faraday.response :logger                  # log requests to STDOUT
          faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        end

        @connection.post do |req|
          req.url client_url
          req.headers['Content-Type'] = 'application/json'
          req.body = API::V1::InvoiceSerializer.new(invoice).attributes.as_json
        end
      end
    end
  end
end