require 'xendit_api'

class API::V1::CreateXenditInvoice < ActiveInteraction::Base
  object :invoice
  string :external_id
  string :payer_email
  string :description
  string :amount  

  def execute
    light = Stoplight('create-xendit-invoice') { create_xendit_invoice }
      .with_threshold(3).with_timeout(2)

    xendit_invoice = light.run

    if light.color != 'green'
      errors.add(:invoice, 'failed request to invoice provider')
    else
      operation = invoice.update(
        provider_invoice_id: xendit_invoice.id,
        status: xendit_invoice.status, 
        invoice_url: xendit_invoice.invoice_url,
        amount: xendit_invoice.amount
      )

      notify_via_slack if operation
      invoice
    end
  end

  private

  def create_xendit_invoice
    client = XenditApi::Client.new(api_key: ENV['XENDIT_SECRET_KEY'])

    client.create_invoice(
      external_id: external_id,
      payer_email: payer_email,
      description: description,
      amount: amount.to_i          
    )
  end

  def notify_via_slack
    compose(
      SendMessageToSlack, 
      message: 'Successfully create Xendit Invoice and update record 
      from database'
    )
  end 
end