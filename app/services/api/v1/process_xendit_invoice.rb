class API::V1::ProcessXenditInvoice < ActiveInteraction::Base
  string :id
  string :status
  string :payer_email
  integer :paid_amount
  integer :adjusted_received_amount
  integer :adjusted_xendit_fee_amount
  date_time :updated

  def execute
    @invoice = Invoice.find_by(provider_invoice_id: id)

    execute_transaction
    return nil if @payment.nil?

    @payment
  end

  private

  def execute_transaction
    ActiveRecord::Base.transaction do
      @invoice.update!(status: status)
      
      @payment = register_payment
      @payment.save!
    end    
  end

  def register_payment
    Payment.new(
      trade_id: @invoice.trade_id,
      sender_identifier_type: 0,
      sender_identifier: payer_email,
      total_paid: paid_amount,
      fees_paid: adjusted_xendit_fee_amount,
      net_paid: adjusted_received_amount,
      currency: 'idr',
      paid_at: updated
    )
  end  
end