class API::V1::TradeRecording < ActiveInteraction::Base
  # Declares event object, preventing the need of Event.new
  object :trade
  # The data input
  string :type
  hash :attributes do
    string   :object_name
    string   :object_identifier
    string   :callback_url
    integer  :initiator
    integer  :status
    string   :invoice_currency
    integer  :invoice_provider

    hash :invoice_data, default: nil do
      string :payer_email
      string :description
      string :amount
    end
  end

  def execute
    raise 'Wrong type of resource' if type != 'trades'
    assigning_attributes

    executing_database_transaction
    return errors.merge!(@exception.record.errors) if @exception

    send_request_to_create_xendit_invoice

    # Return the result
    trade
  end

  private

  def executing_database_transaction
    ActiveRecord::Base.transaction(requires_new: true) do
      begin        
        trade.save!

        @invoice.trade = trade
        @invoice.save!
      rescue ActiveRecord::RecordInvalid => @exception
      end
    end
  end

  def assigning_attributes
    trade.attributes = {
      object_name: attr_val(:object_name), 
      object_identifier: attr_val(:object_identifier),
      callback_url: attr_val(:callback_url), 
      initiator: attr_val(:initiator), 
      status: attr_val(:status)
    }

    @invoice = Invoice.new(
      currency: attr_val(:invoice_currency), 
      amount:  attr_val(:invoice_data)[:amount], 
      provider: attr_val(:invoice_provider)      
    )
  end

  def attr_val(key)
    attributes[key]
  end

  def send_request_to_create_xendit_invoice
    compose(
      API::V1::CreateXenditInvoice, 
      invoice: @invoice, 
      external_id:  @invoice.trade_label,
      payer_email:  attr_val(:invoice_data)[:payer_email],
      description:  attr_val(:invoice_data)[:description],
      amount:       attr_val(:invoice_data)[:amount]
    )
  end
end