require 'slack-notifier'

class SendMessageToSlack < ActiveInteraction::Base
  string :message
  string :slack_webhook_url, default: ENV['SLACK_WEBHOOK_URL']
  string :slack_notif_username, default: ENV['SLACK_NOTIF_USERNAME']

  validates :message, :slack_webhook_url, :slack_notif_username, 
            presence: true

  def execute
    client = Slack::Notifier.new(
      slack_webhook_url,
      username: slack_notif_username
    )

    client.ping(message)
  end
end