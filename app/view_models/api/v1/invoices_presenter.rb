module API::V1
  class InvoicesPresenter < CollectionPresenter
    def initialize(controller:, filters: nil)
      @controller = controller
      @collection = API::V1::InvoicesFilter.new(filters).results
    end
  end
end
