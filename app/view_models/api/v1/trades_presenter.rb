module API::V1
  class TradesPresenter < CollectionPresenter
    def initialize(controller:, filters: nil)
      @controller = controller
      @collection = API::V1::TradesFilter.new(filters).results
    end
  end
end
