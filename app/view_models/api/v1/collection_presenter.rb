module API::V1
  class CollectionPresenter
    DEFAULT_SORTING = { created_at: :desc }
    # SORTABLE_FIELDS = [keys that can be used to sort]
    PER_PAGE = 100

    delegate :params, to: :controller
    delegate :request, to: :controller

    attr_reader :controller

    def initialize(controller:)
      @controller = controller
      @collection = raise NotImplementedError
    end

    def data
      sorted = @collection.order(sort_params)
      @data ||= Kaminari.paginate_array(sorted)
                        .page(current_page).per(PER_PAGE)
    end

    def links
      {
        self:  url_formatter(params: rebuild_params),
        first: first_page,
        last:  last_page,
        prev:  prev_page,
        next:  next_page
      }
    end

    def meta
      { total_pages: total_pages }
    end

    private

    def total_pages
      @total_pages ||= data.total_pages
    end

    def current_page
      return params[:page][:number].to_i if params[:page]
      1
    end

    def first_page
      page = { page: { number: 1 } }
      url_formatter params: rebuild_params.merge(page)
    end

    def next_page
      page = { page: { number: [total_pages, current_page + 1].min } }

      return nil if current_page == total_pages

      url_formatter params: rebuild_params.merge(page)
    end

    def prev_page
      page = { page: { number: [1, current_page - 1].max } }

      return nil if current_page == 1

      url_formatter params: rebuild_params.merge(page)
    end

    def last_page
      page = { page: { number: total_pages } }
      url_formatter params: rebuild_params.merge(page)
    end

    def sort_params
      API::V1::SortParams.sorted_fields(params[:sort], [], DEFAULT_SORTING)
    end

    def rebuild_params
      @rebuild_params ||= begin
        rejected = ['action', 'controller']
        params.to_unsafe_h.reject { |key, _value| rejected.include?(key.to_s) }
      end
    end

    def url_formatter(params:)
      request.original_url.to_s + '/' + params.to_s
    end
  end
end
