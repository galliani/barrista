module API
  module V1
    class InvoiceSerializer < BaseSerializer
      attributes  :provider, :provider_invoice_id, :amount, 
                  :currency, :invoice_url, :status

      link(:self) { api_v1_invoice_url(object) }
    end
  end
end