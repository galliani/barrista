module API
  module V1
    class TradeSerializer < BaseSerializer
      attributes :object_name, :object_identifier, 
                 :initiator, 
                 :status, 
                 :callback_url

      has_one :invoice, serializer: InvoiceSerializer

      link(:self) { api_v1_trade_url(object) }
    end
  end
end