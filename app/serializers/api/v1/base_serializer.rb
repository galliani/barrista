module API
  module V1
    class BaseSerializer < ActiveModel::Serializer
      def base_url
        ENV['HOST_URL']
      end
    end
  end
end
