module API
  module V1
    class PaymentSerializer < BaseSerializer
      attributes  :sender_identifier, :sender_identifier_type, 
                  :recipient_identifier,  :recipient_identifier_type,
                  :currency, :paid_at, 
                  :net_paid, :fees_paid, :total_paid

      has_one :trade, serializer: TradeSerializer

      link(:self) { api_v1_payment_url(object) }
    end
  end
end