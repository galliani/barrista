module API::V1
  class TradesFilter
    attr_reader :results

    def initialize(filters = {})
      @filters = filters
    end

    def results
      results = collections
      unless @filters.nil?
        results = results.where(partner_id: partner) if partner.present?
      end
      results
    end

    def partner
      @filters[:partner_id]
    end

    def collections
      Trade.all
    end
  end
end
